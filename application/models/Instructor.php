<?php

    class Instructor extends CI_Model
    {
        function __construct()
        {
            parent::__construct();


        }
        function insertar($datos){
            //consultar active record en CodeIgniter a los 2 lados (1 hoja) poner graficos
            //INYECTION SQL 1 HOJA PARA LOS 2 CONSULTAS
            return $this->db
                ->insert("instructor",
                $datos);

        }
        //FUNCION PARA CONSULTAR INSTRUCTORES
        function obtenerTodos(){
            //segun active record para ser seguro al injection sql
            $listadoInstructores=
            $this->db->get("instructor");
            //en caso ode que se encuetre vacio
            if($listadoInstructores->num_rows()>0){
                return $listadoInstructores->result();
            }else{
                return false;
            }
        }
        //BORRAR Instructores
        //"id_ins" de la b.dd
        //$id_ins del formulario
        function borrar($id_ins)
        {
          //delete from instructor where id_ins=1 // pero puede hacerse sql injection
          $this->db->where("id_ins",$id_ins);
          if ($this->db->delete("instructor")){
            return true;
          } else {
            return false;
          }
        }
        //FUNCION PARA CONSULTAR INSTRUCTOR ESPECIFICOS
        function obtenerPorId($id_ins)
        {
          $this->db->where("id_ins",$id_ins);
          $instructor=$this->db->get("instructor");
          if($instructor->num_rows()>0){
            return $instructor->row();
          }
          return false;
        }
        //FUNCION PARA ACTUALIZAR UN INSTRUCOTR
        function actualizar($id_ins, $datosEditados)
        {
          $this->db->where("id_ins",$id_ins);
          return $this->db->update('instructor', $datosEditados);
        }
        //otra forma de hacer el borrar
        // function borrar($id_ins)
        // {
        //   $this->db->where("id_ind",$id_ins);
        //   return $this->db->delete("instructor");
        // }
    }//CIERRE DE LA CLASE

?>
