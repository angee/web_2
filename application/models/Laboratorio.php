<?php

    class Laboratorio extends CI_Model
    {
        function __construct()
        {
            parent::__construct();


        }
        function insertar($datos){
            //consultar active record en CodeIgniter a los 2 lados (1 hoja) poner graficos
            //INYECTION SQL 1 HOJA PARA LOS 2 CONSULTAS
            return $this->db
                ->insert("laboratorio",
                $datos);

        }
        //FUNCION PARA CONSULTAR laboratorioES
        function obtenerTodos(){
            //segun active record para ser seguro al injection sql
            $listadolaboratorios=
            $this->db->get("laboratorio");
            //en caso ode que se encuetre vacio
            if($listadolaboratorios->num_rows()>0){
                return $listadolaboratorios->result();
            }else{
                return false;
            }
        }
        //BORRAR laboratorioes
        //"id_lab" de la b.dd
        //$id_lab del formulario
        function borrar($id_lab)
        {
          //delete from laboratorio where id_lab=1 // pero puede hacerse sql injection
          $this->db->where("id_lab",$id_lab);
          if ($this->db->delete("laboratorio")){
            return true;
          } else {
            return false;
          }
        }
        //FUNCION PARA CONSULTAR laboratorio ESPECIFICOS
        function obtenerPorId($id_lab)
        {
          $this->db->where("id_lab",$id_lab);
          $laboratorio=$this->db->get("laboratorio");
          if($laboratorio->num_rows()>0){
            return $laboratorio->row();
          }
          return false;
        }
        //FUNCION PARA ACTUALIZAR UN INSTRUCOTR
        function actualizar($id_lab, $datosEditados)
        {
          $this->db->where("id_lab",$id_lab);
          return $this->db->update('laboratorio', $datosEditados);
        }
        //otra forma de hacer el borrar
        // function borrar($id_lab)
        // {
        //   $this->db->where("id_ind",$id_lab);
        //   return $this->db->delete("laboratorio");
        // }
    }//CIERRE DE LA CLASE

?>
