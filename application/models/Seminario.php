<?php

    class Seminario extends CI_Model
    {
        function __construct()
        {
            parent::__construct();


        }
        function insertar($datos){
            //consultar active record en CodeIgniter a los 2 lados (1 hoja) poner graficos
            //INYECTION SQL 1 HOJA PARA LOS 2 CONSULTAS
            return $this->db
                ->insert("seminario_argg",
                $datos);

        }
        //FUNCION PARA CONSULTAR INSTRUCTORES
        function obtenerTodos(){
            //segun active record para ser seguro al injection sql
            $listadoSeminarios=
            $this->db->get("seminario_argg");
            //en caso ode que se encuetre vacio
            if($listadoSeminarios->num_rows()>0){
                return $listadoSeminarios->result();
            }else{
                return false;
            }
        }
        //BORRAR Instructores
        //"id_ins" de la b.dd
        //$id_ins del formulario
        function borrar($id_argg)
        {
          //delete from instructor where id_ins=1 // pero puede hacerse sql injection
          $this->db->where("id_argg",$id_argg);
          if ($this->db->delete("seminario_argg")){
            return true;
          } else {
            return false;
          }
        }
        //FUNCION PARA CONSULTAR INSTRUCTOR ESPECIFICOS
        function obtenerPorId($id_argg)
        {
          $this->db->where("id_argg",$id_argg);
          $seminario_argg=$this->db->get("seminario_argg");
          if($seminario_argg->num_rows()>0){
            return $seminario_argg->row();
          }
          return false;
        }
        //FUNCION PARA ACTUALIZAR UN INSTRUCOTR
        function actualizar($id_argg, $datosEditados)
        {
          $this->db->where("id_argg",$id_argg);
          return $this->db->update('seminario_argg', $datosEditados);
        }
        //otra forma de hacer el borrar
        // function borrar($id_ins)
        // {
        //   $this->db->where("id_ind",$id_ins);
        //   return $this->db->delete("instructor");
        // }
    }//CIERRE DE LA CLASE

?>
