<?php

    class Medico extends CI_Model
    {
        function __construct()
        {
            parent::__construct();


        }
        function insertar($datos){
            //consultar active record en CodeIgniter a los 2 lados (1 hoja) poner graficos
            //INYECTION SQL 1 HOJA PARA LOS 2 CONSULTAS
            return $this->db
                ->insert("medico",
                $datos);

        }
        function obtenerTodos(){
            //segun active record para ser seguro al injection sql
            $listadoMedicos=
            $this->db->get("medico");
            //en caso ode que se encuetre vacio
            if($listadoMedicos->num_rows()>0){
                return $listadoMedicos->result();
            }else{
                return false;
            }
        }
        //BORRAR Instructores
        //"id_ins" de la b.dd
        //$id_ins del formulario
        function borrar($id_med)
        {
          //delete from instructor where id_ins=1 // pero puede hacerse sql injection
          $this->db->where("id_med",$id_med);
          if ($this->db->delete("medico")){
            return true;
          } else {
            return false;
          }
        }
        function obtenerPorId($id_med)
        {
          $this->db->where("id_med",$id_med);
          $medico=$this->db->get("medico");
          if($medico->num_rows()>0){
            return $medico->row();
          }
          return false;
        }
        //FUNCION PARA ACTUALIZAR UN INSTRUCOTR
       function actualizar($id_med, $datosEditados)
       {
         $this->db->where("id_med",$id_med);
         return $this->db->update('medico', $datosEditados);
       }

    }//CIERRE DE LA CLASE

?>
