<?php

    class Medicos extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            //cargar modelo
            $this->load->model('Medico');
        }
        // FUNCION QUE RENDEREIZA LA VISTA INDEX
        public function index(){
            $data['medicos']=$this->Medico->obtenerTodos();
            // $data['instructores']=$this->Instructor->obtenerTodos();
            $this->load->view('header');
            $this->load->view('medicos/index',$data);
            $this->load->view('footer');
        }
        // FUNCION QUE RENDEREIZA LA VISTA INDEX
        public function nuevo(){
            $this->load->view('header');
            $this->load->view('medicos/nuevo');
            $this->load->view('footer');
        }
        //GET ES INSEGURO  MIENTRAS QUE POST ES MAS SEGURO
        public function guardar(){

            $datosNuevoMedico=array(
                "cedula_med"=>$this->input->post('cedula_med'),
                "apellido_med"=>$this->input->post('apellido_med'),
                "nombre_med"=>$this->input->post('nombre_med'),
                "especialidad_med"=>$this->input->post('especialidad_med'),
                "direccion_med"=>$this->input->post('direccion_med')

            );
            if($this->Medico->insertar($datosNuevoMedico)){
                //con redirect si es true vuelve al formulario despues de insertar datos
                redirect('medicos/index');
            }else{
                echo "<h1> ERROR AL INSERTAR</h1>";
            }

        }
        public function eliminar($id_med)
        {
          // echo $id_ins;
          if ($this->Medico->borrar($id_med)) {
            redirect('medicos/index');
          } else {
            echo "ERROR AL BORRAR";
          }
        }
        //RENDERIZAR VISTA EDITAR CON EL Instructor
        public function editar($id_med)
        {
          $data["medicoEditar"]=$this->Medico->obtenerPorId($id_med);
          $this->load->view('header');
          $this->load->view('medicos/editar',$data);
          $this->load->view('footer');
        }

        public function procesarActualizacion()
        {
          $datosEditados=array(
            "cedula_med"=>$this->input->post('cedula_med'),
            "apellido_med"=>$this->input->post('apellido_med'),
            "nombre_med"=>$this->input->post('nombre_med'),
            "especialidad_med"=>$this->input->post('especialidad_med'),
            "direccion_med"=>$this->input->post('direccion_med')
          );
          $id_med=$this->input->post("id_med");
          if ($this->Medico->actualizar($id_med,$datosEditados)) {
            redirect("medicos/index");
          } else {
            echo "ERROR AL ACTUALIZAR";
          }

        }
    }//CIERRE DE LA CLASE


?>
