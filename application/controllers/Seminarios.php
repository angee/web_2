<?php
  /**
   *
   */
  class Seminarios extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
      //cargar modelo
      $this->load->model('Seminario');
      if(!$this->session->userdata("conectado")){
        redirect("welcome/login");
      }
    }
    public function index(){
    	$this->load->view('header');
    	$this->load->view('seminarios/index');
    	$this->load->view('footer');
  	}
    
    //GET ES INSEGURO  MIENTRAS QUE POST ES MAS SEGURO
    public function insertarSeminario(){
      $datos=array(
       "nombre_argg"=>$this->input->post('nombre_argg'),
       "duracion_argg"=>$this->input->post('duracion_argg'),
       "costo_argg"=>$this->input->post('costo_argg'),
      );
     $this->load->library("upload");
     $new_name = "contenido_seminarios_" . time() . "_" . rand(1, 5000);
     $config['file_name'] = $new_name . '_1';
     $config['upload_path']          = FCPATH . 'uploads/';
     $config['allowed_types']        = 'pdf';
     $config['max_size']             = 1024*5;
     $this->upload->initialize($config);

     if ($this->upload->do_upload("contenido_argg")) {
       $dataSubida = $this->upload->data();
       $datos["contenido_argg"] = $dataSubida['file_name'];
         }
      if($this->Seminario->insertar($datos)){
          $resultado=array("estado"=>"ok");
      }else{
          $resultado=array("estado"=>"error");
      }
     header('Content-Type: application/json');
      echo json_encode($resultado);
    }


    public function listado()
    {
      $data["seminarios_argg"]=$this->Seminario->obtenerTodos();
      $this->load->view("seminarios/listado",$data);
    }

    //funcion para eliminar Seminarioes
    //$id_argg variable de la b.dd
    public function eliminar($id_argg)
    {
        $seminario = $this->Seminario->obtenerPorId($id_argg);
        if ($seminario) {
            $rutaArchivo = FCPATH . 'uploads/' . $seminario->contenido_argg;
            if (file_exists($rutaArchivo)) {
                unlink($rutaArchivo); // Eliminar el archivo
            }
            if ($this->Seminario->borrar($id_argg)) {
                $resultado = array("estado" => "ok");
            } else {
                $resultado = array("estado" => "error");
            }
        } else {
            $resultado = array("estado" => "error");
        }
        header('Content-Type: application/json');
        echo json_encode($resultado);
    }

    //RENDERIZAR VISTA EDITAR CON EL Seminario
    public function editar($id){
      $data["seminarioEditar"]=$this->Seminario->obtenerPorId($id);
        $this->load->view("seminarios/editar",$data);
    }

    //PROCESO DE ACTUALIZACION
    public function procesarActualizacion()
    {
      $datosEditados=array(
        "nombre_argg"=>$this->input->post('nombre_argg'),
        "duracion_argg"=>$this->input->post('duracion_argg'),
        "costo_argg"=>$this->input->post('costo_argg'),
        "contenido_argg"=>$this->input->post('contenido_argg')

      );
      $id_argg=$this->input->post("id_argg");
      if ($this->Seminario->actualizar($id_argg,$datosEditados)) {
        $this->session->set_flashdata("confirmacion", "Seminario ACTULIZADO exitosamente");

      } else {
        $this->session->set_flashdata("error", "Error al ACTUALIZAR, intenta de nuevo");

      }
      redirect("seminarios/index");


    }

  }//fin clase main
?>
