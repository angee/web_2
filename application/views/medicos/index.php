<div class="row">
  <div class="col-md-8">
    <h1><center>LISTADO DE MEDICOS</center></h1>
  </div>
  <div class="col-md-4">
    <a href="<?php echo site_url('medicos/nuevo'); ?>" class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>
      Agregar Medico
    </a>
  </div>
</div>
<div class="container">

    <?php if ($medicos): ?>
        <table class="table table-stripped table-bordered table-hover">
            <thead class="bg-danger">
                <tr>
                    <th>ID</th>
                    <th>CEDULA</th>
                    <th>APELLIDO</th>
                    <th>NOMBRE</th>
                    <th>ESPECIALIDAD</th>
                    <th>DIRECCION</th>
                    <th>ACCIONES</th>
                </tr>
            </thead>
            <tbody class="text-center">
                <?php foreach ($medicos as $filaTemporal): ?>
                    <tr>
                        <td><?php echo $filaTemporal->id_med ?></td>
                        <td><?php echo $filaTemporal->cedula_med ?></td>
                        <td><?php echo $filaTemporal->apellido_med ?></td>
                        <td><?php echo $filaTemporal->nombre_med ?></td>
                        <td><?php echo $filaTemporal->especialidad_med ?></td>
                        <td><?php echo $filaTemporal->direccion_med ?></td>
                        <td class="text-center">

                          <a href="<?php echo site_url(); ?>/medicos/editar/<?php echo $filaTemporal->id_med; ?>" title="Editar Medico" style="color:yellow;">
                            <i class="mdi mdi-pencil"></i>
                            Editar
                          </a>
                          &nbsp;&nbsp;&nbsp;

                          <a href="<?php echo site_url(); ?>/medicos/eliminar/<?php echo $filaTemporal->id_med; ?>"
                            title="Eliminar Medico" style="color:red;"
                            onclick="return confirm('Estas seguro?');"
                            style="color:red;">
                            <i class="mdi mdi-close"></i>
                            Eliminar
                          </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else:?>
    <h1>NO HAY DATOS</h1>
    <?php endif;?>

</div>
