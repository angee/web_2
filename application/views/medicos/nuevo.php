<h1>NUEVO MEDICO</h1>
<form class="" id="frm_nuevo_medico"
action="<?php echo site_url(); ?>/medicos/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
            <span class="Obligatorio">(Obligatorio)</span>

          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          min="99999999"
          name="cedula_med" value=""
          id="cedula_med">
      </div>
      <div class="col-md-4">
          <label for="">Apellido:</label>
            <span class="Obligatorio">(Obligatorio)</span>

          <br>
          <input type="text"
          placeholder="Ingrese su apellido"
          class="form-control"
          name="apellido_med" value=""
          id=apellido_med>
      </div>
      <div class="col-md-4">
        <label for="">Nombre:</label>
          <span class="Obligatorio">(Obligatorio)</span>

        <br>
        <input type="text"
        placeholder="Ingrese su nombre"
        class="form-control"
        name="nombre_med" value=""
        id="nombre_med">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-6">
          <label for="">Especialidad:</label>
            <span class="Obligatorio">(Obligatorio)</span>

          <br>
          <input type="text"
          placeholder="Ingrese su especialidad"
          class="form-control"
          name="especialidad_med" value=""
          id="especialidad_med">
      </div>
      <div class="col-md-6">
          <label for="">Dirección:</label>
            <span class="Obligatorio">(Obligatorio)</span>

          <br>
          <input type="text"
          placeholder="Ingrese su dirección"
          class="form-control"
          name="direccion_med" value=""
          id="direccion_med">
      </div>

    </div>

    <br>


    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/medicos/index"
              class="btn btn-danger">
              Cancelar
            </a>

        </div>
    </div>
</form>

<script type="text/javascript">
  $("#frm_nuevo_medico").validate({
    rules:{
      cedula_med:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true
      },
      apellido_med:{
        required:true,
        minlength:3,
        maxlength:150,
        letras:true
      },
      nombre_med:{
        required:true,
        minlength:3,
        maxlength:150
      },
      especialidad_med:{
        required:true,
        minlength:3,
        maxlength:150
      },
      direccion_med:{
        required:true,
        minlength:3,
        maxlength:150
      },
    },
    messages:{
      cedula_med:{
        required:"Por favor Ingrese el numero de cedula",
        minlength:"Cedula incorrecta, ingrese 10 digitos",
        maxlength:"Cedula incorrecta, ingrese 10 digitos",
        number:"Este campo solo acepta numeros"
      },
      apellido_med:{
        required:"Por favor ingrese el apellido del instructor",
        minlength:"El apellido debe tener al menos 3 caracteres",
        maxlength:"Apellido incorrecto"

      },
      nombre_med:{
        required:"Por favor ingresa tu nombre",
        minlength:"El nombre debe tener al menos 3 caracteres",
        maxlength:"Nombre incorrecto"

      },
      especialidad_med:{
        required:"Por favor ingresa tu titulo",
        minlength:"El titulo debe tener al menos 3 caracteres",
        maxlength:"Titulo incorrecto"
      },
      direccion_med:{
        required:"Por favor ingresa tu direccion",
        minlength:"La direccion debe tener al menos 3 caracteres",
        maxlength:"Direccion incorrecto"
      }
    }
  });
</script>
