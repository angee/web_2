<h1>EDITAR MEDICO</h1>
<form class=""id="frm_editar_medico" action="<?php echo site_url('medicos/procesarActualizacion'); ?>"
method="post">
    <div class="row">
      <div class="col-md-4">
        <label for="" class="form-label">ID</label>
        <input type="text" name="id_med" value="<?php echo $medicoEditar->id_med; ?>" id="id_med" class="form-label">

      </div>
    </div>
    <div class="row">

      <div class="col-md-4">
          <label for="">Cédula:</label>
            <span class="Obligatorio">(Obligatorio)</span>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          required
          min="99999999"
          name="cedula_med" value="<?php echo $medicoEditar->cedula_med ?>"
          id="cedula_med">
      </div>
      <div class="col-md-4">
          <label for="">Apellido:
            <span class="Obligatorio">(Obligatorio)</span>
          </label>

          <br>
          <input type="text"
          placeholder="Ingrese el primer apellido"
          class="form-control"
          required
          name="apellido_med" value="<?php echo $medicoEditar->apellido_med ?>"
          id="apellido_med">
      </div>
      <div class="col-md-4">
        <label for="">Nombre:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el segundo apellido"
        class="form-control"
        name="nombre_med" value="<?php echo $medicoEditar->nombre_med ?>"
        id="nombre_med">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-6">
          <label for="">Especialidad:
            <span class="Obligatorio">(Obligatorio)</span>

          </label>
          <br>
          <input type="text"
          placeholder="Ingrese los nombres"
          class="form-control"
          required
          name="especialidad_med" value="<?php echo $medicoEditar->especialidad_med ?>"
          id="especialidad_med">
      </div>
      <div class="col-md-6">
          <label for="">Dirección:
            <span class="Obligatorio">(Obligatorio)</span>

          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el titulo"
          class="form-control"
          required
          name="direccion_med" value="<?php echo $medicoEditar->direccion_med ?>"
          id="direccion_med">
      </div>

    </div>



    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Editar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/medicos/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
<script type="text/javascript">
  $("#frm_editar_medico").validate({
    rules:{
      cedula_med:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true
      },
      apellido_med:{
        required:true,
        minlength:3,
        maxlength:150,
        letras:true
      },
      nombre_med:{
        required:true,
        minlength:3,
        maxlength:150
      },
      especialidad_med:{
        required:true,
        minlength:3,
        maxlength:150
      },
      direccion_med:{
        required:true,
        minlength:3,
        maxlength:150
      },
    },
    messages:{
      cedula_med:{
        required:"Por favor Ingrese el numero de cedula",
        minlength:"Cedula incorrecta, ingrese 10 digitos",
        maxlength:"Cedula incorrecta, ingrese 10 digitos",
        number:"Este campo solo acepta numeros"
      },
      apellido_med:{
        required:"Por favor ingrese el apellido del instructor",
        minlength:"El apellido debe tener al menos 3 caracteres",
        maxlength:"Apellido incorrecto"

      },
      nombre_med:{
        required:"Por favor ingresa tu nombre",
        minlength:"El nombre debe tener al menos 3 caracteres",
        maxlength:"Nombre incorrecto"

      },
      especialidad_med:{
        required:"Por favor ingresa tu titulo",
        minlength:"El titulo debe tener al menos 3 caracteres",
        maxlength:"Titulo incorrecto"
      },
      direccion_med:{
        required:"Por favor ingresa tu direccion",
        minlength:"La direccion debe tener al menos 3 caracteres",
        maxlength:"Direccion incorrecto"
      }
    }
  });
</script>
