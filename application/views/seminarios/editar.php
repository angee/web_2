<h1>EDITAR SEMINARIO</h1>
<form class=""id="frm_editar_seminario" action="<?php echo site_url('seminarios/procesarActualizacion'); ?>"
method="post" enctype="multipart/form-data">
    <div class="row">
      <div class="col-md-4">
        <!-- <label class="form-label">ID</label> -->
        <input type="hidden" name="id_argg" value="<?php echo $seminarioEditar->id_argg; ?>" id="id_argg" class="form-label">

      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
          <label for="">Nombre:
            <span class="Obligatorio">(Obligatorio)</span>
          </label>

          <br>
          <input type="text"
          placeholder="Ingrese el noombre del seminario"
          class="form-control"
          required
          name="nombre_argg" value="<?php echo $seminarioEditar->nombre_argg ?>"
          id="nombre_argg">
      </div>
      <div class="col-md-6">
        <label for="">Duración:</label>
        <br>
        <input type="text"
        placeholder="Ingrese la duración del seminario"
        class="form-control"
        name="duracion_argg" value="<?php echo $seminarioEditar->duracion_argg ?>"
        id="duracion_argg">
      </div>
    </div>

    <br>
    <div class="row">
      <div class="col-md-12">
          <label for="">Costo:
            <span class="Obligatorio">(Obligatorio)</span>

          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el costo del seminario"
          class="form-control"
          required
          name="costo_argg" value="<?php echo $seminarioEditar->costo_argg ?>"
          id="costo_argg">
      </div>
    </div>

    <br>
    <div class="row">
      <div class="col-md-6">
        <label for="">Contenido</label>
        <br>
        <input type="file" name="contenido_argg" id="contenido_argg" value="">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Editar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/seminarios/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
<script type="text/javascript">
  $("#frm_editar_seminario").validate({
    rules:{
      nombre_argg:{
        required:true,
        minlength:3,
        maxlength:150,
        letras:true
      },
      duracion_argg:{
        required:true,
        minlength:3,
        maxlength:150
      },
      costo_argg:{
        required:true,
        minlength:3,
        maxlength:150
      },
    },
    messages:{
      nombre_argg:{
        required:"Por favor ingrese el nombre del seminario",
        minlength:"El nombre debe tener al menos 3 caracteres",
        maxlength:"Seminario incorrecto"

      },
      duracion_argg:{
        required:"Por favor ingrese la duración del seminario",
        minlength:"La duración debe tener al menos 3 caracteres",
        maxlength:"Duración incorrecta"
      },
      costo_argg:{
        required:"Por favor ingrese el costo del seminario",
        minlength:"El costo debe tener al menos 3 caracteres",
        maxlength:"Costo incorrecto"
      }
    }
  });
</script>

<!-- nuevo para el uploads -->
<script type="text/javascript">
  $("#contenido_argg").fileinput({
    language: "es",
    // allowedFileExtensions: ["jpg", "png", "gif"]
  });
</script>
