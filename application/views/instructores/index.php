
<!-- nuevo -->
<div class="row">
  <div class="col-md-8">
    <h1><center>LISTADO DE INSTRUCTORES</center></h1>
  </div>
  <div class="col-md-4">
    <a href="<?php echo site_url('instructores/nuevo'); ?>" class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>
      Agregar Instructor
    </a>
  </div>
</div>
<!-- NUEVO FIN -->


<div class="container">

    <?php if ($instructores): ?>
        <table class="table table-stripped table-bordered table-hover" id="tbl_instructores">
            <thead class="bg-danger">
                <tr>
                    <th>ID</th>
                    <th>FOTO</th>
                    <th>CEDULA</th>
                    <th>PRIMER APELLIDO</th>
                    <th>SEGUNDO APELLIDO</th>
                    <th>NOMBRES</th>
                    <th>TITULO</th>
                    <th>TELEFONO</th>
                    <th>DIRECCION</th>
                    <th>ACCIONES</th>
                </tr>
            </thead>
            <tbody class="text-center">
                <?php foreach ($instructores as $filaTemporal): ?>
                    <tr>
                        <td><?php echo $filaTemporal->id_ins ?></td>
                        <td>
                          <?php if($filaTemporal->foto_ins!=""): ?>
                            <img src="<?php echo base_url('uploads/').$filaTemporal->foto_ins; ?>" alt="">

                          <?php else: ?>
                            N/A
                          <?php endif; ?>
                        </td>

                        <td><?php echo $filaTemporal->cedula_ins ?></td>
                        <td><?php echo $filaTemporal->primer_apellido_ins ?></td>
                        <td><?php echo $filaTemporal->segundo_apellido_ins ?></td>
                        <td><?php echo $filaTemporal->nombres_ins ?></td>
                        <td><?php echo $filaTemporal->titulo_ins ?></td>
                        <td><?php echo $filaTemporal->telefono_ins ?></td>
                        <td><?php echo $filaTemporal->direccion_ins ?></td>
                        <td class="text-center">
                          <a href="<?php echo site_url(); ?>/instructores/editar/<?php echo $filaTemporal->id_ins; ?>" title="Editar Instructor" style="color:yellow;">
                            <i class="mdi mdi-pencil"></i>
                            Editar
                          </a>
                          &nbsp;&nbsp;&nbsp;
                        <?php if ($this->session->userdata("conectado")->perfil_usu=="ADMINISTRADOR"): ?>
                          <a href="<?php echo site_url(); ?>/instructores/eliminar/<?php echo $filaTemporal->id_ins; ?>"
                            title="Eliminar Instructor" style="color:red;"
                            onclick="return confirm('Estas seguro?');"
                            style="color:red;">
                            <i class="mdi mdi-close"></i>
                            Eliminar
                          </a>
                        <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else:?>
    <h1>NO HAY DATOS</h1>
    <?php endif;?>

</div>


<!-- CODIGO NUEVO -->
<script type="text/javascript">
  $("#tbl_instructores").DataTable();
</script>
