<h3>LISTADO DE LABORATORIOS</h3>
<h6>holaaa</h6>

<?php if ($laboratorios): ?>
  <table class="table">
  	<thead>
    	<tr>
      	<th>ID</th>
      	<th>NOMBRE</th>
      	<th>ACCIONES</th>
    	</tr>
  	</thead>
  	<tbody>
    	<?php foreach ($laboratorios as $lab): ?>
      	<tr>
        	<td><?php echo $lab->id_lab; ?></td>
        	<td><?php echo $lab->nombre_lab; ?></td>
        	<td>
            	Editar &nbsp;
            	Eliminar
        	</td>
      	</tr>
    	<?php endforeach; ?>
  	</tbody>
  </table>
<?php else: ?>
<h1>NO HAY DATOS</h1>
<?php endif; ?>
